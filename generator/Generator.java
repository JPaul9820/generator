
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * All thanks to vilsol, changed a little bit, trying to make it abstract/a platform
 */
public abstract class Generator<T>
{
    Enum type;
    T t;

    public static List<Object> typeModifiers = new ArrayList<>();

    private boolean reRoll;

    private ItemStack item;
    private ItemStack original;

    public Generator(Enum type, T t)
    {
        if (!t.getClass().isEnum())
        {
            throw new IllegalArgumentException();
        }

        this.t = t;
        this.type = type;
    }

    public Generator() {}

    public void setType(Enum type)
    {
        this.type = type;
    }

    public Enum getType()
    {
        return type;
    }

    public void setReRoll(boolean reRoll)
    {
        this.reRoll = reRoll;
    }

    public void setOriginal(ItemStack original)
    {
        this.original = original;
    }

    public ItemStack getItem()
    {
        return item;
    }

    public ItemStack generateItem(@Nullable Enum typeOverride, UUID uuid)
    {
        if (type != null)
        {
            type = typeOverride;
        }

        Random random = new Random();

        ItemStack item = null;

        if (reRoll && original != null)
        {
            item = original;
        }
        else
        {
            try
            {

                Method method = t.getClass().getMethod("getMatFromType", t.getClass());
                Material material = (Material) method.invoke(null, t);
                item = new ItemStack(material);
            } catch (NoSuchMethodException e)
            {
                e.printStackTrace();
            } catch (InvocationTargetException e)
            {
                e.printStackTrace();
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }

        }

        ItemMeta meta = item.getItemMeta().clone();

        if (!reRoll)
        {
            meta.setLore(new ArrayList<>());
        }
        else
        {
            meta.setLore(meta.getLore().subList(0, 4));
        }

        HashMap<ModifierCondition, Modifier> conditions = new HashMap<>();


        List<ModifierCondition> order = new ArrayList<>();

        for (Object object : Arrays.asList(conditions.keySet().toArray()))
        {
            ModifierCondition modifierCondition = (ModifierCondition) object;

            if (!modifierCondition.canApply(conditions.keySet()))
            {

                conditions.remove(modifierCondition);
            }
            else
            {
                Modifier itemModifier = conditions.get(modifierCondition);

                int belowChance = (modifierCondition.getChance() < 0) ? itemModifier.getChance() : modifierCondition.getChance();

                if (random.nextInt(100) < belowChance)
                {
                    order.add(modifierCondition);
                }
                else
                {
                    conditions.remove(modifierCondition);
                }
            }
        }

        for (Modifier itemModifier : conditions.values())
        {
            for (ModifierCondition modifierCondition : (List<ModifierCondition>) ((ArrayList<ModifierCondition>) order).clone())
            {
                if (!modifierCondition.canHave(itemModifier.getClass()))
                {
                    order.remove(modifierCondition);
                }
            }
        }

        Collections.sort(order, new Comparator<ModifierCondition>()
        {
            @Override
            public int compare(ModifierCondition modifierCondition1, ModifierCondition modifierCondition2)
            {
                return conditions.get(modifierCondition1).getOrderPriority() - conditions.get(modifierCondition2).getOrderPriority();
            }
        });

        String name = "";

        for (ModifierCondition modifierCondition : order)
        {
            Modifier itemModifier = conditions.get(modifierCondition);
            meta = itemModifier.applyModifier(modifierCondition, meta);
        }

        List<String> lore = meta.getLore();
        lore.add(type.name());
        lore.add(ChatColor.RED + "Rarity" + ChatColor.RESET + ": " + Strings.capitalizeFirst(rarity.name().toLowerCase()));

        meta.setLore(lore);

        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

        meta.setDisplayName(name);
        item.setItemMeta(meta);
        this.item = item;

        AttributeStorage storage = AttributeStorage.newTarget(item, uuid);
        storage.setData(uuid.toString());

        return storage.getTarget();
    }
}
