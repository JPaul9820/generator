

import java.util.concurrent.ThreadLocalRandom;

/**
 * All thanks to vilsol
 */
public class ModifierRange
{
    ModifierType modifierType;
    double low;
    int lowHigh;
    double high;
    boolean half;

    String random;

    public ModifierRange(ModifierType modifierType, int low, int high)
    {
        this.modifierType = modifierType;
        this.low = low;
        this.high = high;
    }

    public ModifierRange(ModifierType modifierType, int low, int high, boolean half)
    {
        this.modifierType = modifierType;
        this.low = low;
        this.high = high;
        this.half = half;
    }


    public ModifierRange(ModifierType modifierType, int low, int lowHigh, int high)
    {
        this.modifierType = modifierType;
        this.low = low;
        this.lowHigh = lowHigh;
        this.high = high;
    }

    public String generateRandom()
    {
        String random = "";
        ThreadLocalRandom r = ThreadLocalRandom.current();

        double first = (high - low > 0 ? r.nextDouble(high - low) + low : low);
        double second = high;

        if (modifierType == ModifierType.RANGE)
        {

            if (high - first > 0)
            {
                second = r.nextInt((int) (high - first)) + first;
            }

            random += String.valueOf(first);
            random += " - ";
            random += String.valueOf(second);

        }
        else if (modifierType == ModifierType.TRIPLE)
        {

            if (lowHigh - low > 0)
            {
                first = r.nextInt((int) (lowHigh - low)) + low;
            }
            else
            {
                first = low;
            }

            if (high - first > 0)
            {
                second = r.nextInt((int) (high - first)) + first;
            }

            random += String.valueOf(first) + " - " + String.valueOf(second);

        }
        else
        {
            if (half)
            {
                if (first / 2 >= 1)
                {
                    random += String.valueOf(first / 2);
                }
                else
                {
                    random += "1";
                }
            }
            else
            {
                random += String.valueOf(first);
            }
        }

        double roundedRandom = Integers.round(Double.parseDouble(random), 1);

        this.random = String.valueOf(roundedRandom);
        return String.valueOf(roundedRandom);
    }

    public String getRandom()
    {
        return random;
    }
}
